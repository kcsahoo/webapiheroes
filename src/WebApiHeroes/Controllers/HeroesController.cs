﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using WebApiHeroes.Models;
using WebApiHeroes.Contexts;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiHeroes.Controllers
{
        [Route("api/[controller]")]
        //[EnableCors("AllowSpecificOrigin")]
        public class HeroesController : Controller
        {

            private HeroesDbContext _context;

            public HeroesController(HeroesDbContext context)
            {
                _context = context;
            }

            // GET: api/heroes
            [HttpGet]
            //[EnableCors("AllowSpecificOrigin")]
            public async Task<ObjectResult> Get()
            {
                var heroes= _context.Heroes.ToListAsync();
            return Ok(heroes);
        }

            // GET api/heroes/5
            [HttpGet("{id}")]
            public async Task<ObjectResult> Get(int id)
            {
                var hero= _context.Heroes.Where(h=>h.id==id).ToListAsync();
                return Ok(hero);
            }

            // POST api/heroes
            [HttpPost]
            //[EnableCors("AllowSpecificOrigin")]
            public async Task<ObjectResult> Post([FromBody] Heroes hero)
            {
                _context.Heroes.Add(hero);
                _context.Entry(hero).State = EntityState.Added;
                await _context.SaveChangesAsync();
                return Ok(hero);
            }

            // PUT api/heroes/5
            [HttpPut("{id}")]
            //[EnableCors("AllowSpecificOrigin")]
            public async Task<ObjectResult> Put([FromBody] Heroes hero)
            {
                _context.Entry(hero).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Ok(hero);
        }

            // PATCH api/heroes/5
            [HttpPatch("{id}")]
            //[EnableCors("AllowSpecificOrigin")]
            public Heroes Patch(int id, [FromBody] string name)
            {
                var heroExisting = _context.Heroes.ToList().Find(h => h.id == id);
                heroExisting.Name = name;
                _context.SaveChanges();
                return heroExisting;
            }

            // DELETE api/heroes/5
            [HttpDelete("{id}")]
            //[EnableCors("AllowSpecificOrigin")]
            public async Task Delete(int id)
                {
                var product = await _context.Heroes
                                .SingleOrDefaultAsync(e => e.id == id);
                if (product == null) return;

                _context.Entry(product).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
            }
        }
}
