﻿using System.Data.Entity;
using WebApiHeroes.Models;

namespace WebApiHeroes.Contexts
{
    [DbConfigurationType(typeof(DbConfig))]
    public class HeroesDbContext : DbContext
    {
        public HeroesDbContext(string connectionName) :base(connectionName) { }

        public DbSet<Heroes> Heroes { get; set; }
    }
}
