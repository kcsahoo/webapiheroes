﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiHeroes.Models
{
    public class Heroes
    {
        public int id { get; set; } //must exist
        public string Name { get; set; }
    }
}
